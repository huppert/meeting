package com.none.meeting.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.none.meeting.R;
import com.none.meeting.entities.VotingResult;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VotingResultLayout extends LinearLayout {

    @BindView(R.id.tv_option1)
    TextView tvOption1;
    @BindView(R.id.tv_count1)
    TextView tvCount1;
    @BindView(R.id.tv_option2)
    TextView tvOption2;
    @BindView(R.id.tv_count2)
    TextView tvCount2;
    @BindView(R.id.btn_refresh)
    Button btnRefresh;

    public VotingResultLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_voting_result, this);
        ButterKnife.bind(this, view);

    }

    public void initData(VotingResult votingResult) {
        tvOption1.setText(votingResult.getAgree().getTitle());
        tvOption2.setText(votingResult.getDisagree().getTitle());

        int count1 = votingResult.getAgree().getCount();
        int count2 = votingResult.getDisagree().getCount();
        int total = count1 + count2;

        tvCount1.setText(count1 + "人");
        tvCount2.setText(count2 + "人");

    }

    public Button getBtnRefresh() {
        return btnRefresh;
    }
}
