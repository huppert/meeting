package com.none.meeting.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;

import com.alivc.player.AliVcMediaPlayer;
import com.alivc.player.MediaPlayer;
import com.alivc.player.VcPlayerLog;
import com.none.meeting.R;
import com.none.meeting.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WatchActivity extends BaseActivity {
    private static final String TAG = "WatchActivity";
    @BindView(R.id.fl_root)
    FrameLayout flRoot;
    private SurfaceView mSurfaceView;
    private AliVcMediaPlayer mAliVcMediaPlayer;
    private String mUrl = "rtmp://192.168.254.104/livestream/1234";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch);
        ButterKnife.bind(this);
        init();
    }

    protected void init() {
        initSurfaceView();

    }


    /**
     * 初始化播放器显示view
     */
    private void initSurfaceView() {
        flRoot.setKeepScreenOn(true);
        mSurfaceView = new SurfaceView(this);
        addSubView(mSurfaceView);

        SurfaceHolder holder = mSurfaceView.getHolder();
        //增加surfaceView的监听
        holder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                VcPlayerLog.d(TAG, " surfaceCreated = surfaceHolder = " + surfaceHolder);
                initAliVcPlayer();
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width,
                                       int height) {
                VcPlayerLog.d(TAG,
                        " surfaceChanged surfaceHolder = " + surfaceHolder + " ,  width = " + width + " , height = "
                                + height);
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                VcPlayerLog.d(TAG, " surfaceDestroyed = surfaceHolder = " + surfaceHolder);
            }
        });
    }


    /**
     * addSubView 添加子view到布局中
     *
     * @param view 子view
     */
    private void addSubView(View view) {
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        flRoot.addView(view, params);//添加到布局中
    }

    private void initAliVcPlayer() {
        mAliVcMediaPlayer = new AliVcMediaPlayer(this, mSurfaceView);
        mAliVcMediaPlayer.setRefer("");
        mAliVcMediaPlayer.setBusinessId("");
        mAliVcMediaPlayer.setMuteMode(true);
        mAliVcMediaPlayer.setVideoScalingMode(MediaPlayer.VideoScalingMode.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);

        mAliVcMediaPlayer.setPreparedListener(() -> Log.d(TAG, "onPrepared: "));
        mAliVcMediaPlayer.setErrorListener((i, msg) -> {
            //错误发生时触发，错误码见接口文档
            Log.d(TAG, "onError: " + msg);
        });
        mAliVcMediaPlayer.setCompletedListener(() -> {
            //视频正常播放完成时触发
            Log.d(TAG, "onCompleted: ");
        });

        mAliVcMediaPlayer.prepareAndPlay(mUrl);

    }


    @Override
    public void onStop() {
        super.onStop();
        if (mAliVcMediaPlayer != null) {
            mAliVcMediaPlayer.stop();
        }
    }


    @Override
    protected void onDestroy() {
        flRoot.setKeepScreenOn(false);
        if (mAliVcMediaPlayer != null) {
            mAliVcMediaPlayer.destroy();
        }
        super.onDestroy();
    }

}
