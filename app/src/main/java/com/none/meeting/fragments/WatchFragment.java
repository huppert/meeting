package com.none.meeting.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.alivc.player.AliVcMediaPlayer;
import com.alivc.player.MediaPlayer;
import com.alivc.player.VcPlayerLog;
import com.none.meeting.R;
import com.none.meeting.api.ApiService;
import com.none.meeting.base.BaseFragment;
import com.none.meeting.common.HttpRequest;
import com.none.meeting.common.UserHelper;
import com.none.meeting.entities.LiveRoom;
import com.none.meeting.util.RxUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;

public class WatchFragment extends BaseFragment {

    @BindView(R.id.fl_root)
    FrameLayout flRoot;
    private SurfaceView mSurfaceView;
    private AliVcMediaPlayer mAliVcMediaPlayer;
    private final ApiService service = HttpRequest.createService(ApiService.class);
    private String mUrl;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_watch, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    protected void init() {
        initSurfaceView();
        getRoomInfo();
    }


    /**
     * 初始化播放器显示view
     */
    private void initSurfaceView() {
        rootView.setKeepScreenOn(true);
        mSurfaceView = new SurfaceView(getContext());
        addSubView(mSurfaceView);

        SurfaceHolder holder = mSurfaceView.getHolder();
        //增加surfaceView的监听
        holder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                VcPlayerLog.d(TAG, " surfaceCreated = surfaceHolder = " + surfaceHolder);
                initAliVcPlayer();
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width,
                                       int height) {
                VcPlayerLog.d(TAG,
                        " surfaceChanged surfaceHolder = " + surfaceHolder + " ,  width = " + width + " , height = "
                                + height);
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                VcPlayerLog.d(TAG, " surfaceDestroyed = surfaceHolder = " + surfaceHolder);
            }
        });
    }


    /**
     * addSubView 添加子view到布局中
     *
     * @param view 子view
     */
    private void addSubView(View view) {
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        flRoot.addView(view, params);//添加到布局中
    }

    private void initAliVcPlayer() {
        mAliVcMediaPlayer = new AliVcMediaPlayer(getContext(), mSurfaceView);
        mAliVcMediaPlayer.setRefer("");
        mAliVcMediaPlayer.setBusinessId("");
        mAliVcMediaPlayer.setMuteMode(true);
        mAliVcMediaPlayer.setVideoScalingMode(MediaPlayer.VideoScalingMode.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);

        mAliVcMediaPlayer.setPreparedListener(() -> Log.d(TAG, "onPrepared: "));
        mAliVcMediaPlayer.setErrorListener((i, msg) -> {
            //错误发生时触发，错误码见接口文档
            Log.d(TAG, "onError: " + msg);
        });
        mAliVcMediaPlayer.setCompletedListener(() -> {
            //视频正常播放完成时触发
            Log.d(TAG, "onCompleted: ");
        });

    }


    private void getRoomInfo() {
        Disposable disposable = service.getRoomInfo()
                .compose(RxUtil.applySchedulers())
                .filter(resp -> !handleResponse(resp))
                .subscribe(response -> {
                    LiveRoom liveRoom = response.getData()[0];
                    //主讲人
                    if (UserHelper.getUser().getUserInfo().getUsertype() == 1) {
                        mUrl = liveRoom.getPush_url();
                    } else {
                        mUrl = liveRoom.getPull_url();
                    }

                    mAliVcMediaPlayer.prepareAndPlay(mUrl);


                }, this::handleFailure);
        mCompositeDisposable.add(disposable);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAliVcMediaPlayer != null) {
            mAliVcMediaPlayer.stop();
        }
    }

    @Override
    public void onDestroyView() {
        rootView.setKeepScreenOn(false);
        if (mAliVcMediaPlayer != null) {
            mAliVcMediaPlayer.destroy();
        }
        super.onDestroyView();

    }
}
