package com.none.meeting.api;

import com.none.meeting.base.response.BaseResponse;
import com.none.meeting.entities.LiveRoom;
import com.none.meeting.entities.Meeting;
import com.none.meeting.entities.UserResp;
import com.none.meeting.entities.Voting;
import com.none.meeting.entities.VotingResult;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {

    /**
     * 登录
     */
    @GET("user/login/{username}/{password}")
    Observable<BaseResponse<UserResp>> login(@Path("username") String username,
                                             @Path("password") String password);

    /**
     * 获取会议全部投票信息
     */
    @GET("votes/get/{meetingid}")
    Observable<BaseResponse<List<Voting>>> getVotingList(@Path("meetingid") int id);


    /**
     * 查看投票结果
     */
    @GET("meeting/votes/get/{meetingid}/{voteid}")
    Observable<BaseResponse<VotingResult>> getVotingResult(@Path("meetingid") int meetingId,
                                                           @Path("voteid") int vid);


    /**
     * 投票
     *
     * @param opinion 1 同意 2 不同意
     */

    @GET("votes/set/{meetingid}/{voteid}/{userid}/{opinion}")
    Observable<BaseResponse<VotingResult>> vote(@Path("meetingid") int meetingId,
                                                @Path("voteid") int votingId,
                                                @Path("userid") int userId,
                                                @Path("opinion") String opinion);

    /**
     * 会议详情
     */
    @GET("meeting/list/get/{id}")
    Observable<BaseResponse<List<Meeting>>> loadMeeting(@Path("id") int id);

    /**
     * 直播室
     */

    @GET("meeting/rooms/get")
    Observable<BaseResponse<LiveRoom[]>> getRoomInfo();
}
