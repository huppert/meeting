package com.none.meeting.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.none.meeting.R;
import com.none.meeting.base.BaseActivity;
import com.none.meeting.common.FragmentAdapter;
import com.none.meeting.common.UserHelper;
import com.none.meeting.fragments.LiveFragment;
import com.none.meeting.fragments.MeetingDetailFragment;
import com.none.meeting.fragments.VoteFragment;
import com.none.meeting.fragments.WatchFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {
    private static final String TAG = "MainActivity";

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.item_meeting_info)
    FrameLayout itemMeetingInfo;
    @BindView(R.id.item_meeting_room)
    FrameLayout itemMeetingRoom;
    @BindView(R.id.item_voting)
    FrameLayout itemVoting;
    private FrameLayout[] navViews = new FrameLayout[3];
    private FragmentAdapter fragmentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        navViews[0] = itemMeetingInfo;
        navViews[1] = itemMeetingRoom;
        navViews[2] = itemVoting;
        selectNav(itemMeetingInfo);
        init();
    }

    private void init() {
        Fragment[] fragments = new Fragment[]{
                new MeetingDetailFragment(),
                UserHelper.getUser().getUserInfo().getUsertype() == 1 ? new LiveFragment() : new WatchFragment(),
                new VoteFragment()
        };
        fragmentAdapter = new FragmentAdapter(getSupportFragmentManager(), fragments, R.id.fl_content);
    }

    @OnClick({R.id.item_meeting_info, R.id.item_meeting_room, R.id.item_voting})
    public void onViewClicked(View view) {
        selectNav(view);
        switch (view.getId()) {
            case R.id.item_meeting_info:
                tvTitle.setText(R.string.meeting_info);
                fragmentAdapter.showFragment(0);
                break;
            case R.id.item_meeting_room:
                tvTitle.setText(R.string.meeting_room);
                fragmentAdapter.showFragment(1);
                break;
            case R.id.item_voting:
                tvTitle.setText(R.string.vote);
                fragmentAdapter.showFragment(2);
                break;
        }
    }


    private void selectNav(View view) {
        for (FrameLayout navView : navViews) {
            navView.setSelected(view == navView);
        }
    }

}
