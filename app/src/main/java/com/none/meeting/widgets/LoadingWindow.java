package com.none.meeting.widgets;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.none.meeting.R;


public class LoadingWindow extends PopupWindow {

    private Context mContext;
    private String mContent;

    public LoadingWindow(Context context) {
        super(context);
        mContext = context;
    }

    public LoadingWindow(Context context, String content) {
        super(context);
        mContext = context;
        mContent = content;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public void show() {
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_ios_loading, null);
        setContentView(contentView);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        setOutsideTouchable(false);
        setFocusable(true);
        showAtLocation(contentView, Gravity.CENTER, 0, 0);
        TextView content = contentView.findViewById(R.id.tv_content);
        IndicatorImageView view = contentView.findViewById(R.id.iv_loading);
        view.startAnimator();
        if (TextUtils.isEmpty(mContent)) {
            content.setVisibility(View.GONE);
        } else {
            content.setVisibility(View.VISIBLE);
            content.setText(mContent);
        }

    }

}
