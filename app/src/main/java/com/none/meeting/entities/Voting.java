package com.none.meeting.entities;

public class Voting {

    private int id;
    private int meeting_id;
    private String title;
    private int poll_rst;
    private int writerid;
    private int poll_userid;
    private String created_at;
    private String updated_at;
    private String true_1;
    private String false_2;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMeeting_id() {
        return meeting_id;
    }

    public void setMeeting_id(int meeting_id) {
        this.meeting_id = meeting_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPoll_rst() {
        return poll_rst;
    }

    public void setPoll_rst(int poll_rst) {
        this.poll_rst = poll_rst;
    }

    public int getWriterid() {
        return writerid;
    }

    public void setWriterid(int writerid) {
        this.writerid = writerid;
    }

    public int getPoll_userid() {
        return poll_userid;
    }

    public void setPoll_userid(int poll_userid) {
        this.poll_userid = poll_userid;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getTrue_1() {
        return true_1;
    }

    public String getFalse_2() {
        return false_2;
    }
}
