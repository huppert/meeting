package com.none.meeting.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.none.meeting.R;
import com.none.meeting.base.BaseAdapter;
import com.none.meeting.entities.Meeting;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MeetingAdapter extends BaseAdapter<Meeting> {


    public MeetingAdapter(List<Meeting> list, Context context) {
        super(list, context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MeetingViewHolder(mInflater.inflate(R.layout.item_meeting, parent, false));
    }


    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        MeetingViewHolder holder = (MeetingViewHolder) viewHolder;
        Meeting meeting = mList.get(position);
        holder.tvMeetingTitle.setText(meeting.getTitle());
        holder.tvPublishDate.setText(mContext.getString(R.string.publish_date_format, meeting.getCreated_at()));
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(mOnClickListener);
    }

    static class MeetingViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_meeting_title)
        TextView tvMeetingTitle;
        @BindView(R.id.tv_publish_date)
        TextView tvPublishDate;

        public MeetingViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
