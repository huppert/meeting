package com.none.meeting.base;


import androidx.annotation.StringRes;


public interface BaseView {
    void showToast(CharSequence text);

    void showToast(@StringRes int resId);

    void showProgress(@StringRes int resId);

    void showProgress(String msg);

    void dismissWindow();

    void handleFailure(Throwable e);
}
