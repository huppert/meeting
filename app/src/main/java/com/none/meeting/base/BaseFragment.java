package com.none.meeting.base;


import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;

import com.none.meeting.base.response.BaseResponse;

import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseFragment extends Fragment implements BaseView {
    protected View rootView;
    protected boolean isInit;
    protected Unbinder mUnBinder;
    protected CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    protected String TAG;
    private RespHandler mRespHandler;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = getClass().getSimpleName();
        mRespHandler = new RespHandler(getContext());
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (rootView == null || isInit) return;
            init();
            isInit = true;
        }
    }

    protected abstract void init();


    @Override
    public void showToast(CharSequence text) {
        mRespHandler.showToast(text);
    }

    public void showToast(@StringRes int resId) {
        mRespHandler.showToast(resId);
    }

    public void showProgress(@StringRes int resId) {
        showProgress(getString(resId));
    }

    public void showProgress(String msg) {
        mRespHandler.showLoadingWindow(msg);
    }

    public void dismissWindow() {
        mRespHandler.dismissWindow();
    }


    public boolean handleResponse(BaseResponse response) {
        return mRespHandler.handleResponse(response);
    }

    public void handleFailure(Throwable e) {
        mRespHandler.handleFailure(e);
    }


    @Override
    public void onDestroyView() {
        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
        mCompositeDisposable.clear();
        super.onDestroyView();
    }


}
