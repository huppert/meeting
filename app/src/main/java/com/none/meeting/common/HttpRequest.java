package com.none.meeting.common;


import com.none.meeting.BuildConfig;
import com.none.meeting.MeetingApplication;
import com.none.meeting.util.LogTrace;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * 网络请求类
 */
public class HttpRequest {
    private volatile static OkHttpClient sOkHttpClient;
    private volatile static Retrofit sRetrofit;
    private static final long HTTP_DISK_CACHE_MAX_SIZE = 10 * 1024 * 1024;

    public static OkHttpClient getInstance() {
        if (sOkHttpClient == null) {
            synchronized (HttpRequest.class) {
                if (sOkHttpClient == null) {
                    sOkHttpClient = new OkHttpClient.Builder()
                            .cache(new Cache(new File(MeetingApplication.getInstance().getCacheDir(), "httpCache"), HTTP_DISK_CACHE_MAX_SIZE))
                            .addInterceptor(new LoggingInterceptor())
                            .build();
                }
            }
        }
        return sOkHttpClient;
    }


    public static <T> T createService(Class<T> clazz) {
        if (sRetrofit == null) {
            sRetrofit = new Retrofit.Builder()
                    .baseUrl("http://pay.plumcp.cc/server.php/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(getInstance())
                    .build();
        }
        return sRetrofit.create(clazz);
    }


    private static class LoggingInterceptor implements Interceptor {

        private static final String TAG = "LoggingInterceptor.class";

        @Override
        public Response intercept(Chain chain) throws IOException {
            //release版本不打印log
            if (!BuildConfig.DEBUG) {
                return chain.proceed(chain.request());
            }
            Request original = chain.request();
            long t1 = System.nanoTime();
            LogTrace.d(TAG, String.format(Locale.CHINA, "intercept:Sending original %s on %s%n%s",
                    original.url(), chain.connection(), original.headers()));
            Response response = chain.proceed(original);
            long t2 = System.nanoTime();
            LogTrace.d(TAG, String.format(Locale.CHINA, "intercept:Received response for %s in %.1fms%n%s",
                    response.request().url(), (t2 - t1) / 1e6d, response.headers()));

            String body = response.body().string();
            LogTrace.d(TAG, "bodyString = " + body);
            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), body))
                    .build();
        }
    }


}
