package com.none.meeting.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.none.meeting.R;
import com.none.meeting.api.ApiService;
import com.none.meeting.base.BaseFragment;
import com.none.meeting.common.HttpRequest;
import com.none.meeting.common.UserHelper;
import com.none.meeting.entities.Meeting;
import com.none.meeting.util.RxUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;

public class MeetingDetailFragment extends BaseFragment {


    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_publish_date)
    TextView tvPublishDate;
    @BindView(R.id.tv_content)
    TextView tvContent;
    private final ApiService service = HttpRequest.createService(ApiService.class);


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meeting_detail, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        init();
        return view;
    }


    private void loadMeeting() {
        Disposable disposable = service.loadMeeting(UserHelper.getUser().getMeetingid())
                .compose(RxUtil.applySchedulers())
                .filter(resp -> !handleResponse(resp))
                .subscribe(response -> {
                    List<Meeting> data = response.getData();
                    if (data == null || data.size() == 0) {
                        showToast("当前没有会议");
                    } else {
                        initData(data.get(0));
                    }
                }, this::handleFailure);
        mCompositeDisposable.add(disposable);
    }

    private void initData(Meeting meeting) {
        tvTitle.setText(meeting.getTitle());
        tvPublishDate.setText(getString(R.string.publish_date_format, meeting.getCreated_at()));
        tvContent.setText(meeting.getContext());
    }

    @Override
    protected void init() {
        isInit = true;
        loadMeeting();
    }
}
