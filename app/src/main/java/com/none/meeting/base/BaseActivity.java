package com.none.meeting.base;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import com.none.meeting.base.response.BaseResponse;
import com.none.meeting.common.ActivityStackManager;

import java.lang.ref.WeakReference;

import io.reactivex.disposables.CompositeDisposable;


public class BaseActivity extends AppCompatActivity implements BaseView {
    protected TextView mTvTitle;
    protected TextView mBack;
    protected CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    protected RespHandler mRespHandler;
    protected boolean mLockFlag = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityStackManager.add(new WeakReference<>(this).get());
        mRespHandler = new RespHandler(this);
    }


    public void showToast(CharSequence text) {
        mRespHandler.showToast(text);
    }

    public void showToast(@StringRes int resId) {
        mRespHandler.showToast(resId);
    }

    public void showProgress(@StringRes int resId) {
        showProgress(getString(resId));
    }

    public void showProgress(String msg) {
        mRespHandler.showLoadingWindow(msg);
    }

    public void dismissWindow() {
        mRespHandler.dismissWindow();
    }


    public boolean handleResponse(BaseResponse response) {
        return mRespHandler.handleResponse(response);
    }

    public void handleFailure(Throwable e) {
        mRespHandler.handleFailure(e);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityStackManager.remove(this);
        mCompositeDisposable.clear();
    }
}
