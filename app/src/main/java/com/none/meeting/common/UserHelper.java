package com.none.meeting.common;

import com.none.meeting.entities.UserResp;

public class UserHelper {
    private static UserResp sUser;

    public static void setUser(UserResp user) {
        sUser = user;
    }

    public static UserResp getUser() {
        return sUser;
    }
}
