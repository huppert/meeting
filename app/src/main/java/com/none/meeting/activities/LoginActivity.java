package com.none.meeting.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.PermissionChecker;

import com.none.meeting.R;
import com.none.meeting.api.ApiService;
import com.none.meeting.base.BaseActivity;
import com.none.meeting.common.HttpRequest;
import com.none.meeting.common.UserHelper;
import com.none.meeting.entities.UserResp;
import com.none.meeting.util.RxUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.Disposable;

public class LoginActivity extends BaseActivity {

    private final ApiService service = HttpRequest.createService(ApiService.class);
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.iv_clear_username)
    ImageView ivClearUsername;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.iv_clear_pwd)
    ImageView ivClearPwd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        etUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ivClearUsername.setVisibility(TextUtils.isEmpty(s) ? View.INVISIBLE : View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ivClearPwd.setVisibility(TextUtils.isEmpty(s) ? View.INVISIBLE : View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (!permissionCheck()) {
            if (Build.VERSION.SDK_INT >= 23) {
                ActivityCompat.requestPermissions(this, permissionManifest, PERMISSION_REQUEST_CODE);
            } else {
                showNoPermissionTip(getString(noPermissionTip[mNoPermissionIndex]));
                finish();
            }
        }


        etUsername.setText("username1");
        etPassword.setText("password1");
    }


    private int mNoPermissionIndex = 0;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private final String[] permissionManifest = {
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    private final int[] noPermissionTip = {
            R.string.no_camera_permission,
            R.string.no_record_audio_permission,
            R.string.no_write_external_storage_permission,
    };

    private boolean permissionCheck() {
        int permissionCheck = PackageManager.PERMISSION_GRANTED;
        String permission;
        for (int i = 0; i < permissionManifest.length; i++) {
            permission = permissionManifest[i];
            mNoPermissionIndex = i;
            if (PermissionChecker.checkSelfPermission(this, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                permissionCheck = PackageManager.PERMISSION_DENIED;
            }
        }
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    private void showNoPermissionTip(String tip) {
        Toast.makeText(this, tip, Toast.LENGTH_LONG).show();
    }

    @OnClick({R.id.iv_clear_username, R.id.iv_clear_pwd, R.id.btn_login})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_clear_username:
                etUsername.setText(null);
                break;
            case R.id.iv_clear_pwd:
                etPassword.setText(null);
                break;
            case R.id.btn_login:
                if (validate()) {
                    showProgress("登录中");
                    login();
                }

                break;
        }
    }


    private boolean validate() {
        if (TextUtils.isEmpty(getUserName())) {
            showToast("用户名不能为空");
            return false;
        }
        if (TextUtils.isEmpty(getPassword())) {
            showToast("密码不能为空");
            return false;
        }
        return true;
    }

    private void login() {
        Disposable disposable = service.login(getUserName(), getPassword())
                .compose(RxUtil.applySchedulers())
                .filter(resp -> !handleResponse(resp))
                .subscribe(response -> {
                    UserResp userResp = response.getData();
                    UserHelper.setUser(userResp);
                    dismissWindow();
                    startActivity(new Intent(this, MainActivity.class));
                    finish();
                }, this::handleFailure);
        mCompositeDisposable.add(disposable);

    }


    private String getUserName() {
        return etUsername.getText().toString();
    }

    private String getPassword() {
        return etPassword.getText().toString();
    }
}
