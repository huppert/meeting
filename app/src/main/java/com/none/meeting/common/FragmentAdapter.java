package com.none.meeting.common;


import androidx.annotation.IdRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class FragmentAdapter {
    private Fragment[] mFragments;
    private FragmentManager mFragmentManager;

    public FragmentAdapter(FragmentManager fragmentManager, Fragment[] fragments, @IdRes int contentId) {
        this.mFragments = fragments;
        this.mFragmentManager = fragmentManager;
        init(contentId);
    }

    private void init(int contentId) {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        for (int i = 0; i < mFragments.length; i++) {
            fragmentTransaction.add(contentId, mFragments[i]);
        }
        fragmentTransaction.commit();
        showFragment(0);
    }

    public void showFragment(int position) {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        for (int i = 0; i < mFragments.length; i++) {
            if (i == position) {
                mFragments[i].setUserVisibleHint(true);
                fragmentTransaction.show(mFragments[i]);
            } else {
                mFragments[i].setUserVisibleHint(false);
                fragmentTransaction.hide(mFragments[i]);
            }
        }
        fragmentTransaction.commit();
    }
}
