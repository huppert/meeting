package com.none.meeting.activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.alivc.live.pusher.AlivcLivePushConfig;
import com.alivc.live.pusher.AlivcLivePushInfoListener;
import com.alivc.live.pusher.AlivcLivePusher;
import com.alivc.live.pusher.AlivcPreviewOrientationEnum;
import com.alivc.live.pusher.AlivcQualityModeEnum;
import com.none.meeting.R;
import com.none.meeting.base.BaseActivity;
import com.none.meeting.floatwindowpermission.FloatWindowManager;
import com.none.meeting.util.VideoRecordViewManager;

import java.util.ArrayList;
import java.util.List;

public class LiveTestActivity extends BaseActivity {
    private static final String TAG = "LiveFragment";

    private static final int REQ_CODE_PERMISSION = 0x1111;

    public static final int CAPTURE_PERMISSION_REQUEST_CODE = 0x1123;
    public static final int OVERLAY_PERMISSION_REQUEST_CODE = 0x1124;

    private AlivcLivePushConfig mAlivcLivePushConfig;
    private AlivcLivePusher mAlivcLivePusher = null;
    private long mStartTime;


    private Button btnStart;
    private String mUrl = "rtmp://192.168.254.104/live/livestream";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_test);
        getWindow().getDecorView().setKeepScreenOn(true);
        btnStart = findViewById(R.id.btn_live);
        init();
        initView();
    }

    protected void init() {
        mAlivcLivePushConfig = new AlivcLivePushConfig();
        mAlivcLivePushConfig.setQualityMode(AlivcQualityModeEnum.QM_FLUENCY_FIRST);
        mAlivcLivePushConfig.setBeautyOn(false);
//        mAlivcLivePushConfig.setVideoOnly(true);
//        mAlivcLivePushConfig.setNetworkPoorPushImage(Environment.getExternalStorageDirectory().getPath() + File.separator + "alivc_resource/poor_network_land.png");
//        mAlivcLivePushConfig.setPausePushImage(Environment.getExternalStorageDirectory().getPath() + File.separator + "alivc_resource/background_push_land.png");


        AlivcLivePushConfig.setMediaProjectionPermissionResultData(null);
        mUrl = "rtmp://192.168.254.104/livestream/1234";

    }


    private void initView() {
        btnStart.setOnClickListener(v -> {
            if (mAlivcLivePusher == null) {
                if (FloatWindowManager.getInstance().applyFloatWindow(getContext())) {
                    startScreenCapture();
                    btnStart.setText("结束会议");
                }
            } else {
                stopPushWithoutSurface();
                btnStart.setText(R.string.start_meeting);
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQ_CODE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // User agree the permission
                    startPushWithoutSurface(mUrl);
                } else {
                    // User disagree the permission
                    Toast.makeText(getContext(), "请先开启相机权限", Toast.LENGTH_LONG).show();
                }
            }
            break;
            default:
                break;
        }
    }

    private Context getContext() {
        return this;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!isHome()) {
            VideoRecordViewManager.hideViewRecordWindow();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (isHome()) {
            if (mAlivcLivePusher != null && mAlivcLivePusher.isPushing()) {
                VideoRecordViewManager.createViewoRecordWindow(this, getContext().getApplicationContext(), mAlivcLivePusher, cameraOnListener);
                VideoRecordViewManager.showViewRecordWindow();
                Log.d(TAG, "onStop: 显示后台悬浮窗");
            }
        }
    }

    private VideoRecordViewManager.CameraOn cameraOnListener = on -> {
        if (on) {
            VideoRecordViewManager.createViewoRecordCameraWindow(this, getContext().getApplicationContext(), mAlivcLivePusher);
        } else {
            VideoRecordViewManager.removeVideoRecordCameraWindow(getContext().getApplicationContext());
        }
    };


    @Override
    public void onPause() {
        super.onPause();
        VideoRecordViewManager.refreshFloatWindowPosition();
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAPTURE_PERMISSION_REQUEST_CODE: {
                if (resultCode == Activity.RESULT_OK) {
                    mAlivcLivePushConfig.setMediaProjectionPermissionResultData(data);
                    if (mAlivcLivePushConfig.getMediaProjectionPermissionResultData() != null) {
                        if (mAlivcLivePusher == null) {
                            startPushWithoutSurface(mUrl);
                        } else {
                            stopPushWithoutSurface();
                        }
                    }
                }
            }
            break;
            case OVERLAY_PERMISSION_REQUEST_CODE:
                break;
            default:
                break;
        }
    }


    @Override
    protected void onDestroy() {
        getWindow().getDecorView().setKeepScreenOn(false);
        VideoRecordViewManager.removeVideoRecordCameraWindow(getContext().getApplicationContext());
        VideoRecordViewManager.removeVideoRecordWindow(getContext().getApplicationContext());
        if (mAlivcLivePusher != null) {
            try {
                mAlivcLivePusher.stopCamera();
            } catch (Exception e) {
            }
            try {
                mAlivcLivePusher.stopCameraMix();
            } catch (Exception e) {

            }
            try {
                mAlivcLivePusher.stopPush();
            } catch (Exception e) {
            }
            try {
                mAlivcLivePusher.stopPreview();
            } catch (Exception e) {
            }
            mAlivcLivePusher.destroy();
            mAlivcLivePusher.setLivePushInfoListener(null);
            mAlivcLivePusher = null;
        }
        super.onDestroy();
    }

    private void startScreenCapture() {
        MediaProjectionManager mediaProjectionManager = (MediaProjectionManager)
                getContext().getSystemService(Context.MEDIA_PROJECTION_SERVICE);
        try {
            this.startActivityForResult(
                    mediaProjectionManager.createScreenCaptureIntent(), CAPTURE_PERMISSION_REQUEST_CODE);
        } catch (ActivityNotFoundException ex) {
            ex.printStackTrace();
            runOnUiThread(() -> Toast.makeText(getContext()
                    , "Start ScreenRecording failed, current device is NOT suuported!", Toast.LENGTH_SHORT).show());
        }
    }

    private void stopPushWithoutSurface() {
        VideoRecordViewManager.removeVideoRecordCameraWindow(getContext().getApplicationContext());
        VideoRecordViewManager.removeVideoRecordWindow(getContext().getApplicationContext());
        if (mAlivcLivePusher != null) {
            try {
                mAlivcLivePusher.stopCamera();
            } catch (Exception e) {
            }
            try {
                mAlivcLivePusher.stopCameraMix();
            } catch (Exception e) {
            }
            try {
                mAlivcLivePusher.stopPush();
            } catch (Exception e) {
            }
            try {
                mAlivcLivePusher.stopPreview();
            } catch (Exception e) {
            }
            try {
                mAlivcLivePusher.destroy();
            } catch (Exception e) {
            }

            mAlivcLivePusher.setLivePushInfoListener(null);
            mAlivcLivePusher = null;
        }

    }

    private void startPushWithoutSurface(String url) {
        mAlivcLivePusher = new AlivcLivePusher();

        try {
            mAlivcLivePusher.init(getContext().getApplicationContext(), mAlivcLivePushConfig);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        mAlivcLivePusher.setLivePushInfoListener(new AlivcLivePushInfoListener() {
            @Override
            public void onPreviewStarted(AlivcLivePusher pusher) {

            }

            @Override
            public void onPreviewStoped(AlivcLivePusher pusher) {

            }

            @Override
            public void onPushStarted(AlivcLivePusher pusher) {
            }

            @Override
            public void onFirstAVFramePushed(AlivcLivePusher pusher) {
                Log.d(TAG, "onFirstAVFramePushed: ");
            }

            @Override
            public void onPushPauesed(AlivcLivePusher pusher) {
                Log.d(TAG, "onPushPauesed: ");

            }

            @Override
            public void onPushResumed(AlivcLivePusher pusher) {

            }

            @Override
            public void onPushStoped(AlivcLivePusher pusher) {

            }

            @Override
            public void onPushRestarted(AlivcLivePusher pusher) {

            }

            @Override
            public void onFirstFramePreviewed(AlivcLivePusher pusher) {

            }

            @Override
            public void onDropFrame(AlivcLivePusher pusher, int countBef, int countAft) {

            }

            @Override
            public void onAdjustBitRate(AlivcLivePusher pusher, int curBr, int targetBr) {

            }

            @Override
            public void onAdjustFps(AlivcLivePusher pusher, int curFps, int targetFps) {

            }
        });

        mAlivcLivePusher.startPreview(null);
        mAlivcLivePusher.startPush(url);
        mStartTime = System.currentTimeMillis();

        mAlivcLivePusher.setMute(true);
        mAlivcLivePusher.setPreviewOrientation(AlivcPreviewOrientationEnum.ORIENTATION_LANDSCAPE_HOME_LEFT);
//        mAlivcLivePusher.setCaptureVolume(mCaptureVolume);

    }

    private boolean isHome() {
        ActivityManager mActivityManager = (ActivityManager) getContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> rti = mActivityManager.getRunningTasks(1);
        return getHomes().contains(rti.get(0).topActivity.getPackageName());
    }

    private List<String> getHomes() {
        List<String> names = new ArrayList<>();
        PackageManager packageManager = getContext().getPackageManager();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        List<ResolveInfo> resolveInfos = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo ri : resolveInfos) {
            names.add(ri.activityInfo.packageName);
        }
        return names;
    }


}
