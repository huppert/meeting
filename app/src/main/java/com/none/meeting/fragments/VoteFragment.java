package com.none.meeting.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.none.meeting.R;
import com.none.meeting.adapter.VotingAdapter;
import com.none.meeting.api.ApiService;
import com.none.meeting.base.BaseFragment;
import com.none.meeting.base.response.BaseResponse;
import com.none.meeting.common.HttpRequest;
import com.none.meeting.common.UserHelper;
import com.none.meeting.entities.Voting;
import com.none.meeting.entities.VotingGroup;
import com.none.meeting.util.RxUtil;
import com.none.meeting.widgets.EndlessRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;

public class VoteFragment extends BaseFragment {
    private final ApiService service = HttpRequest.createService(ApiService.class);
    @BindView(R.id.recyclerView)
    EndlessRecyclerView recyclerView;
    private final List<VotingGroup> mList = new ArrayList<>();
    private int mCurrentPosition;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_voting, container, false);
        mUnBinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    protected void init() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        VotingAdapter adapter = new VotingAdapter(mList, getContext());
        recyclerView.setAdapter(adapter);
        adapter.setOnClickListener(v -> {
            mCurrentPosition = (int) v.getTag();
            VotingGroup group = mList.get(mCurrentPosition);
            switch (v.getId()) {
                case R.id.btn_complete:
                    if (group.getCheckedOption() == -1) {
                        showToast("请选择投票选项");
                    } else {
                        showProgress("提交中");
                        vote(group);
                    }
                    break;
                case R.id.btn_refresh:
                    showProgress("刷新中");
                    getVotingResult(group);
                    break;
            }

        });
        getVotingList();
    }

    /**
     * 投票信息列表
     */
    private void getVotingList() {
        Disposable disposable = service.getVotingList(UserHelper.getUser().getMeetingid())
                .compose(RxUtil.applySchedulers())
                .filter(resp -> !handleResponse(resp))
                .subscribe(response -> {
                    List<Voting> data = response.getData();
                    for (Voting datum : data) {
                        mList.add(new VotingGroup(datum));
                    }
                    recyclerView.getAdapter().notifyDataSetChanged();


                }, this::handleFailure);
        mCompositeDisposable.add(disposable);
    }

    /**
     * 投票
     */
    private void vote(VotingGroup group) {
        int userId = UserHelper.getUser().getUserInfo().getId();
        Voting votingInfo = group.getVotingInfo();
        Disposable disposable = service.vote(votingInfo.getMeeting_id(), votingInfo.getId(), userId, group.getCheckedOption() == 0 ? votingInfo.getTrue_1() : votingInfo.getFalse_2())
                .compose(RxUtil.applySchedulers())
                .filter(resp -> !preHandleResponse(resp))
                .subscribe(response -> {
                    dismissWindow();
                    showToast(response.getMsg());

                    int position = getCurrentPosition(Integer.parseInt(response.getData().getVoteid()));
                    VotingGroup votingGroup = mList.get(position);
                    votingGroup.setBtnEnable(false);
                    votingGroup.setVotingResult(response.getData());
                    recyclerView.getAdapter().notifyItemChanged(position);

                }, this::handleFailure);
        mCompositeDisposable.add(disposable);
    }


    /**
     * 查看投票结果
     */
    private void getVotingResult(VotingGroup group) {
        Voting votingInfo = group.getVotingInfo();
        Disposable disposable = service.getVotingResult(votingInfo.getMeeting_id(), votingInfo.getId())
                .compose(RxUtil.applySchedulers())
                .filter(resp -> !handleResponse(resp))
                .subscribe(response -> {
                    dismissWindow();

                    int position = getCurrentPosition(Integer.parseInt(response.getData().getVoteid()));
                    VotingGroup votingGroup = mList.get(position);
                    votingGroup.setVotingResult(response.getData());
                    recyclerView.getAdapter().notifyItemChanged(position);
                }, this::handleFailure);
        mCompositeDisposable.add(disposable);
    }


    private boolean preHandleResponse(BaseResponse response) {
        if (response.getCode() != 0) {
            dismissWindow();
            switch (response.getCode()) {
                //已经投过票了
                case 1:
                    return false;
                default:
                    showToast(response.getMsg());
                    break;
            }
            return true;
        }
        return false;
    }


    private int getCurrentPosition(int meetingId) {
        for (int i = 0; i < mList.size(); i++) {
            VotingGroup votingGroup = mList.get(i);
            if (meetingId == votingGroup.getVotingInfo().getId()) {
                return i;
            }
        }

        return -1;
    }


}
