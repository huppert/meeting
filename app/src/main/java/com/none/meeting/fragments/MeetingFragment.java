package com.none.meeting.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.none.meeting.R;
import com.none.meeting.adapter.MeetingAdapter;
import com.none.meeting.base.BaseFragment;
import com.none.meeting.entities.Meeting;
import com.none.meeting.widgets.EndlessRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MeetingFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    EndlessRecyclerView recyclerView;
    private final List<Meeting> mList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meeting, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        init();
        return view;
    }

    @Override
    protected void init() {
        isInit = true;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecoration.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.meeting_list_divider));
        recyclerView.addItemDecoration(itemDecoration);
        MeetingAdapter adapter = new MeetingAdapter(mList, getContext());
        adapter.setOnClickListener(v -> {
            int position = (int) v.getTag();
            Meeting meeting = mList.get(position);
            getFragmentManager().beginTransaction()
                    .addToBackStack(null)
                    .commit();
        });
        recyclerView.setAdapter(adapter);
    }


}
