package com.none.meeting.entities;

public class VotingGroup {
    private Voting votingInfo;
    private VotingResult votingResult;
    private int checkedOption = -1;
    private boolean btnEnable = true;

    public VotingGroup(Voting votingInfo) {
        this.votingInfo = votingInfo;
    }

    public Voting getVotingInfo() {
        return votingInfo;
    }

    public void setVotingInfo(Voting votingInfo) {
        this.votingInfo = votingInfo;
    }

    public VotingResult getVotingResult() {
        return votingResult;
    }

    public void setVotingResult(VotingResult votingResult) {
        this.votingResult = votingResult;
    }

    public int getCheckedOption() {
        return checkedOption;
    }

    public void setCheckedOption(int checkedOption) {
        this.checkedOption = checkedOption;
    }

    public boolean isBtnEnable() {
        return btnEnable;
    }

    public void setBtnEnable(boolean btnEnable) {
        this.btnEnable = btnEnable;
    }
}
