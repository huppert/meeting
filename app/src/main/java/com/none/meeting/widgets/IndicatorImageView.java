package com.none.meeting.widgets;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

public class IndicatorImageView extends AppCompatImageView {
    private ObjectAnimator animator;

    public IndicatorImageView(Context context) {
        this(context, null);
    }

    public IndicatorImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        animator = ObjectAnimator.ofFloat(this, "rotation", 360f)
                .setDuration(1500);
        animator.setInterpolator(new LinearInterpolator());
        animator.setRepeatMode(ObjectAnimator.RESTART);
        animator.setRepeatCount(ObjectAnimator.INFINITE);
    }


    public void startAnimator() {
        animator.start();
    }

    public void stopAnimator() {
        animator.cancel();
    }


    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility == VISIBLE) {
            startAnimator();
        } else {
            stopAnimator();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopAnimator();
    }
}
