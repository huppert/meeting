package com.none.meeting.common;


import com.none.meeting.base.BaseActivity;
import com.none.meeting.util.LogTrace;

import java.util.Vector;

/**
 * activity 管理类
 */
public class ActivityStackManager {
    private static final Vector<BaseActivity> activityStack = new Vector<>();

    public static void add(BaseActivity activity) {
        activityStack.add(activity);
    }

    public static void remove(BaseActivity activity) {
        activityStack.remove(activity);
    }

    public static void pop() {
        BaseActivity last = getLast();
        remove(last);
        last.finish();
    }

    public static void clear() {
        activityStack.clear();
    }

    public static BaseActivity getLast() {
        return activityStack.lastElement();
    }

    public static int getSize() {
        return activityStack.size();
    }

    public static void exit() {
        LogTrace.d("ActivityStackManager", "exit: size=" + activityStack.size());
        synchronized (activityStack) {
            for (BaseActivity activity : activityStack) {
                if (activity != null) {
                    LogTrace.d("BaseActivity", "exit: class  =" + activity.getClass());
                    activity.finish();
                }
            }
            clear();
        }
    }

}
