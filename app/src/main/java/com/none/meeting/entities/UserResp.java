package com.none.meeting.entities;

public class UserResp {
    private User userinfo;
    private int meetingid;

    public User getUserInfo() {
        return userinfo;
    }

    public int getMeetingid() {
        return meetingid;
    }
}
