package com.none.meeting.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.none.meeting.util.LogTrace;


public class EndlessRecyclerView extends RecyclerView {
    private static final String TAG = "EndlessRecyclerView";
    private OnLoadMoreListener mOnLoadMoreListener;
    private View emptyView;
    private boolean mLoading;
    /**
     * 当前页数
     */
    private int mCurPage = 1;
    private int mPageCount;

    public EndlessRecyclerView(final Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setOverScrollMode(OVER_SCROLL_NEVER);
        setClipToPadding(false);
        setClipChildren(false);
        setItemAnimator(null);
        this.addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LayoutManager layoutManager = getLayoutManager();
                int visibleCount = layoutManager.getChildCount();
                int count = layoutManager.getItemCount();
                int first = 0;
                //注意顺序，GridLayoutManager继承自LinearLayoutManager
                if (layoutManager instanceof GridLayoutManager) {
                    first = ((GridLayoutManager) layoutManager).findFirstVisibleItemPosition();
                } else if (layoutManager instanceof LinearLayoutManager) {
                    first = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
                }
//                else {//StaggeredGridLayoutManager
//                }

                LogTrace.d(TAG, "onScrolled: first = " + first + "   visibleCount = " + visibleCount + "   count = " + count);
                if (mOnLoadMoreListener != null
                        && first + visibleCount >= count
                        && !mLoading
                        && mCurPage < mPageCount) {
                    mCurPage++;
                    setLoading(true);
                    //https://stackoverflow.com/questions/39445330/cannot-call-notifyiteminserted-method-in-a-scroll-callback-recyclerview-v724-2
                    post(() -> mOnLoadMoreListener.loadMore());
                }
            }
        });

    }


    private void checkIfEmpty() {
        if (emptyView != null) {
            LogTrace.d(TAG, "checkIfEmpty: " + getAdapter().getItemCount());
            if (getAdapter().getItemCount() > 0) {
                emptyView.setVisibility(GONE);
                this.setVisibility(VISIBLE);
            } else {
                emptyView.setVisibility(VISIBLE);
                this.setVisibility(GONE);
            }
        }
    }

    private final AdapterDataObserver observer = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            checkIfEmpty();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
            checkIfEmpty();
        }
    };


    @Override
    public void swapAdapter(Adapter adapter, boolean removeAndRecycleExistingViews) {
        final Adapter oldAdapter = getAdapter();
        if (oldAdapter != null) {
            oldAdapter.unregisterAdapterDataObserver(observer);
        }
        if (adapter != null) {
            adapter.registerAdapterDataObserver(observer);
        }
        super.swapAdapter(adapter, removeAndRecycleExistingViews);
    }

    public void setEmptyView(@Nullable View emptyView) {
        this.emptyView = emptyView;
    }


    public interface OnLoadMoreListener {
        void loadMore();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener listener) {
        mOnLoadMoreListener = listener;
    }


    public boolean isLoading() {
        return mLoading;
    }

    public void setLoading(boolean loading) {
        mLoading = loading;
    }

    public int getCurPage() {
        return mCurPage;
    }

    /**
     * 设置start page
     */
    public void setStartPage(int curPage) {
        mCurPage = curPage;
    }

    public void pageDecrement() {
        mCurPage--;
    }

    public void pageIncrement() {
        mCurPage++;
    }

    public int getPageCount() {
        return mPageCount;
    }


    public void setPageCount(int pageCount) {
        mPageCount = pageCount;
    }

    public boolean isLastPage() {
        return mCurPage == mPageCount && mCurPage > 0;
    }
}
