package com.none.meeting.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.none.meeting.R;
import com.none.meeting.base.BaseAdapter;
import com.none.meeting.entities.Voting;
import com.none.meeting.entities.VotingGroup;
import com.none.meeting.widgets.VotingResultLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VotingAdapter extends BaseAdapter<VotingGroup> {

    private static final String TAG = "VotingAdapter";

    public VotingAdapter(List<VotingGroup> list, Context context) {
        super(list, context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VotingHolder(mInflater.inflate(R.layout.item_voting, parent, false));
    }


    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        VotingHolder holder = (VotingHolder) viewHolder;
        VotingGroup votingGroup = mList.get(position);
        Voting votingInfo = votingGroup.getVotingInfo();
        holder.rbAgree.setText(votingInfo.getTrue_1());
        holder.rbDisagree.setText(votingInfo.getFalse_2());
        holder.tvVotingTitle.setText(votingInfo.getTitle());
        Log.d(TAG, "bindItemViewHolder: " + votingGroup.getCheckedOption() + "     标题" + votingGroup.getVotingInfo().getTitle());
        holder.radioGroup.setOnCheckedChangeListener(null);

        if (votingGroup.getCheckedOption() == -1) {
            holder.radioGroup.clearCheck();
        } else {
            holder.radioGroup.check(votingGroup.getCheckedOption() == 0 ? R.id.rb_agree : R.id.rb_disagree);
        }

        holder.radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            Log.d(TAG, "setOnCheckedChangeListener: checkedId = " + checkedId + "   标题" + votingGroup.getVotingInfo().getTitle());
            if (checkedId != -1) {
                votingGroup.setCheckedOption(checkedId == holder.rbAgree.getId() ? 0 : 1);
            } else {
                votingGroup.setCheckedOption(-1);
            }

        });

        if (votingGroup.getVotingResult() != null) {
            holder.resultLayout.initData(votingGroup.getVotingResult());
            holder.resultLayout.setVisibility(View.VISIBLE);
            holder.resultLayout.getBtnRefresh().setTag(position);
            holder.resultLayout.getBtnRefresh().setOnClickListener(mOnClickListener);
        } else {
            holder.resultLayout.setVisibility(View.INVISIBLE);
        }

        holder.btnComplete.setEnabled(votingGroup.isBtnEnable());
        holder.btnComplete.setText(votingGroup.isBtnEnable() ? R.string.vote : R.string.complete_voting);
        holder.btnComplete.setTag(position);
        holder.btnComplete.setOnClickListener(mOnClickListener);
    }

    static class VotingHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_voting_title)
        TextView tvVotingTitle;
        @BindView(R.id.rb_agree)
        RadioButton rbAgree;
        @BindView(R.id.radio_group)
        RadioGroup radioGroup;
        @BindView(R.id.rb_disagree)
        RadioButton rbDisagree;
        @BindView(R.id.btn_complete)
        Button btnComplete;
        @BindView(R.id.result_layout)
        VotingResultLayout resultLayout;

        public VotingHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
