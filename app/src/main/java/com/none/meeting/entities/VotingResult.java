package com.none.meeting.entities;

public class VotingResult {

    private String meetingid;
    private String voteid;
    private AgreeBean agree;
    private AgreeBean disagree;

    public String getMeetingid() {
        return meetingid;
    }

    public void setMeetingid(String meetingid) {
        this.meetingid = meetingid;
    }

    public String getVoteid() {
        return voteid;
    }

    public void setVoteid(String voteid) {
        this.voteid = voteid;
    }

    public AgreeBean getAgree() {
        return agree;
    }

    public void setAgree(AgreeBean agree) {
        this.agree = agree;
    }

    public AgreeBean getDisagree() {
        return disagree;
    }

    public void setDisagree(AgreeBean disagree) {
        this.disagree = disagree;
    }

    public static class AgreeBean {

        private String title;
        private int count;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }


}
