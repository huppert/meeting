package com.none.meeting.entities;

import android.os.Parcel;
import android.os.Parcelable;

public class Meeting implements Parcelable {

    private int id;
    private String title;
    private String context;
    private int userid;
    private String date_of_meeting;
    private String created_at;
    private String updated_at;

    protected Meeting(Parcel in) {
        id = in.readInt();
        title = in.readString();
        context = in.readString();
        userid = in.readInt();
        date_of_meeting = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(context);
        dest.writeInt(userid);
        dest.writeString(date_of_meeting);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Meeting> CREATOR = new Creator<Meeting>() {
        @Override
        public Meeting createFromParcel(Parcel in) {
            return new Meeting(in);
        }

        @Override
        public Meeting[] newArray(int size) {
            return new Meeting[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContext() {
        return context;
    }

    public int getUserid() {
        return userid;
    }

    public String getDate_of_meeting() {
        return date_of_meeting;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }
}
