package com.none.meeting.base;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.widget.Toast;

import androidx.annotation.StringRes;

import com.none.meeting.base.response.BaseResponse;
import com.none.meeting.widgets.LoadingWindow;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

import retrofit2.HttpException;


public class RespHandler {
    protected LoadingWindow mLoadingWindow;
    protected Toast mToast;
    private Context mContext;
    private static final int NO_AUTHORIZATION = 103;

    public RespHandler(Context context) {
        mContext = context;
    }

    public void showLoadingWindow(String content) {
        if (mLoadingWindow == null) {
            mLoadingWindow = new LoadingWindow(mContext);
        }
        if (mLoadingWindow.isShowing()) {
            mLoadingWindow.dismiss();
        }
        mLoadingWindow.setContent(content);
        mLoadingWindow.show();

    }


    public void dismissWindow() {
        if (mLoadingWindow != null && mLoadingWindow.isShowing()) {
            mLoadingWindow.dismiss();
        }
    }


    public void showToast(CharSequence text) {
        if (mToast == null) {
            mToast = Toast.makeText(mContext, text, Toast.LENGTH_SHORT);
        }
        mToast.setText(text);
        mToast.show();
    }

    public void showToast(@StringRes int resId) {
        showToast(mContext.getResources().getText(resId));
    }


    public boolean handleResponse(BaseResponse response) {
        if (response.getCode() != 0) {
            dismissWindow();
            switch (response.getCode()) {
                default:
                    showToast(response.getMsg());
                    break;
            }
            return true;
        }
        return false;
    }


    /**
     * 处理异常情况
     */
    public void handleFailure(Throwable e) {
        dismissWindow();
        showToast(getErrorMsg(e));
    }

    public String getErrorMsg(Throwable throwable) {
        throwable.printStackTrace();
        String resultMsg = "未知错误";
        if (throwable instanceof HttpException) {
            if (((HttpException) throwable).code() == 401) {
                resultMsg = "请求失败，请重试";
            } else {
                resultMsg = "服务器异常";
            }

        } else if (throwable instanceof ConnectException) {
            resultMsg = "网络连接失败";
        } else if (throwable instanceof NetworkErrorException) {
            resultMsg = "网络错误";
        } else if (throwable instanceof TimeoutException || throwable instanceof SocketTimeoutException) {
            resultMsg = "连接超时";
        } else if (throwable instanceof java.net.UnknownHostException) {
            resultMsg = "无法连接网络,请检查网络设置";
        }
        return resultMsg;
    }
}
